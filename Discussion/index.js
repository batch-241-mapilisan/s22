// Array Methods
// For Array Methods, we have the mutator and non-mutator methods.

// Mutator Methods
/*
- Mutator methods are functions that "mutate" or change an array after they're created.
- These methods manipulate the original array. Performing tasks such as adding and removing elements.
- push(), pop(), unshift(), shift(), splice(), sort(), & reverse()
*/

