const registeredUsers = [];

function register(user) {
    if (registeredUsers.indexOf(user) === -1) {
        registeredUsers.push(user)
        alert("Thank you for registering!")
    } else {
        alert("Registration failed. Username already exists!")
    }
}

const friendsList = [];

function addFriend(user) {
    if (registeredUsers.indexOf(user) !== -1) {
        friendsList.push(user);
        alert(`You have added ${user} as a friend!`)
    } else {
        alert("User not found")
    }
}

function displayFriends() {
    if (friendsList.length > 0) {
        friendsList.forEach(friend => console.log(friend))
    } else {
        alert("You currently have 0 friends. Add on first.")
    }
}

function displayNumberOfFriends() {
    if (friendsList.length > 0) {
        console.log(`You currently have ${friendsList.length} friends.`);
    } else {
        alert("You currently have 0 friends. Add on first.")
    }
}

function deleteFriend() {
    if (friendsList.length > 0) {
        friendsList.pop()
    } else {
        alert("You currently have 0 friends. Add on first.")
    }
}

